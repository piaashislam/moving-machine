import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-produc-details-page',
  templateUrl: './produc-details-page.component.html',
  styleUrls: ['./produc-details-page.component.scss']
})
export class ProducDetailsPageComponent implements OnInit {

  constructor() {

  }
  elements: any = [
    { title: 'Type', spec: '320 t/h' },
    { title: 'MAX output', spec: 'on customer request' },
    { title: 'Cold feeders: capacity and number', spec: 'on customer request' },
    { title: 'Type drying drum	', spec: 'E 250' },
    { title: 'Burner power output', spec: '19 MW' },
    { title: 'Capacity of recevered fines hopper', spec: '	40 t / 80 t' },
    { title: 'Filter surface', spec: '1012 m2' },
    { title: 'Screen surface total area', spec: '37 m2 / 45 m2' },
    { title: 'Screening', spec: '	5 (6 as an option)' },
    { title: 'Hot aggregate bins', spec: '80 t or 140 t' },
    { title: 'Mixer content', spec: '4 t' },
    { title: 'Imported filler Storage', spec: 'on customer request' },
    { title: 'Bitumen storage', spec: '	on customer request' },
    { title: 'HMA storage silo / compartments', spec: 'direct loading' },
    { title: 'Recycling addition up to 35%', spec: 'RAP into dryer drum ring' }
  ];

  headElements = ['title', 'spec'];

  ngOnInit() {
  }

}
