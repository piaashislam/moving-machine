import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-enquiry-page',
  templateUrl: './product-enquiry-page.component.html',
  styleUrls: ['./product-enquiry-page.component.scss']
})
export class ProductEnquiryPageComponent implements OnInit {
  enquiryForm: FormGroup;
  submitted = false;


  constructor(private formBuilder: FormBuilder) { }

  elements: any = [
    { title: 'Articulated Trucks' },
    { title: 'Asphalt Pavers' },
    { title: 'Backhoe Loaders' },
    { title: 'Draglines' },
    { title: 'Scraper' },
    { title: 'Loader' },
    { title: 'Power shovel' },
    { title: 'Bulldozer' },
    { title: 'Track skidder' }
  ];

  headElements = ['title', 'spec'];
  ngOnInit() {
    this.enquiryForm = this.formBuilder.group({
      title: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      // tel: ['', [Validators.required, Validators.minLength(11)]],
      comment: ['', Validators.required],
      tel: ['', [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.minLength(8),
      ]],
      // confirmPassword: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue]
    });
    // this.aFormGroup = this.formBuilder.group({
    //   recaptcha: ['', Validators.required]
    // });
  }

  // convenience getter for easy access to form fields
  get f() { return this.enquiryForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.enquiryForm.invalid) {
      return;
    }

    // display form values on success
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.enquiryForm.value, null, 4));
  }

  onReset() {
    this.submitted = false;
    this.enquiryForm.reset();
  }

}
